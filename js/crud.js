'use strict;'

//var rstcrd = new RestCRUD('http://localhost:7500','/notes');
var arrayOfElements = [];
/**
 * 
 * @param {URL} url adresse du serveur
 * @param {String} ressourceName nom de ressource commencant par /
 */
function RestCRUD(url, ressourceName) {
    if (undefined == url) url = ADRESSE_REST_SRV;
    //Vérification du contenu de ressourcename    
    if (undefined === ressourceName || null === ressourceName) {
        ressourceName = "/notes";
    }

    //const URL = url + ressourceName;
    /*Variables */
    var _self = this;
    const _URL = url + ressourceName;
    var _isTotallyReceived = false;
    this.isFinished = function() {
        return _isTotallyReceived;
    }
    this.lastReceivedObject = undefined;
    this.lastStatus = undefined;
    this.lastErrCode = undefined;

    /**
     * 
     * @param {*} params paramètres d'entrée : method, ressourceUrl, obj
     */
    //function _callxhr(method, ressourceUrl, obj) {
    function _callxhr(params) {
        console.log('Vous venez de demander un appel XHR')
        var xhr = new XMLHttpRequest();
        xhr.open(params.method, params.ressourceUrl, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = function(event) {
            if (this.readyState == this.DONE && (this.status >= 200 && this.status < 300)) {
                //console.log(this.status, JSON.parse(this.response))
                if (undefined !== params.callback) params.callback(this.response);
            }
        }
        if ('POST' === params.method) delete params.obj.id;
        xhr.send((undefined !== params.obj) ? JSON.stringify(params.obj) : undefined);
    }

    this.create = function(objet, callBack) {
        console.log('Create function called')
        _callxhr({
            method: 'POST',
            ressourceUrl: _URL,
            obj: objet,
            callback: callBack
        });
    };

    this.read = function(id) {
        _isTotallyReceived = false;
        console.log('Read function called')
        _callxhr({
            method: 'GET',
            ressourceUrl: (undefined !== id) ? _URL + '/' + id : _URL,
            callback: function(response) {
                _self.lastReceivedObject = parseJSONNote({ jsonstr: response });
                _isTotallyReceived = true;
                //console.log('Request is done ', _self.lastReceivedObject);
                return _self.lastReceivedObject;
            }
        })
        return _self.lastReceivedObject;
    };

    this.update = function(obj) {
        if (undefined === obj) return -1;
        else if (null === obj.id) return -1;
        console.log('Update function called')
        _callxhr({
            method: 'PUT',
            ressourceUrl: _URL + '/' + obj.id,
            obj,
            callback: function(response) {
                _self.lastReceivedObject = parseJSONNote({ jsonstr: response });
                _isTotallyReceived = true;
                console.log('Request is done ', _self.lastReceivedObject);
            }
        });
    };

    this.delete = function(id, clbk) {
        if (undefined == id) return -1;
        console.log('Delete function called')
        _callxhr({ method: 'DELETE', ressourceUrl: _URL + '/' + id, callback: clbk });
    };
}