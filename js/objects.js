 'use strict;'
 // parseJSONNote({obj:{}});
 // parseJSONNote({jsonstr:''});

 /**
  * 
  * @param {*} params 
  */
 function parseJSONNote(params) {
     var datas = (undefined !== params.obj) ? params.obj : JSON.parse(params.jsonstr);
     var isArray = Array.isArray(datas);
     if (!isArray) {
         var date = new Date(datas.date);
         delete datas.heure;
         datas.date = date;
         var note = Object.assign(new Note(), datas);
         makeNote(note);
         return note;
     } else {
         var arrayRet = [];
         //datas.forEach(function(element, index,nbElement){})
         datas.forEach(element => {
             arrayRet.push(parseJSONNote({ obj: element }));
         });
         return arrayRet;
     }
 }

 function mergeFromJSON(jsonstr) {
     var datas = JSON.parse(jsonstr);

 }

 /**
  * 
  * @param {*} id id de la note
  * @param {*} name nom de la note
  * @param {*} date date de la note
  * @param {*} desc description
  * @param {*} user utilisateur
  */
 function Note(id, name, date, desc, user) {
     this.desc = (undefined === desc) ? '' : desc;
     this.user = (undefined === user) ? '' : user;
     this.date = (undefined === date) ? new Date() : date;
     this.name = (undefined === name) ? '' : name;
     if (undefined !== id || '' != id)
         this.id = id;
 }