'use strict;'

function jsinfo(texte, couleur) {
    var bandejs = document.querySelector('#bandeJS');
    bandejs.style.backgroundColor = couleur;
    bandejs.innerText = texte;
}

jsinfo('JS chargé !!', 'GREEN');

function makeNote(note) {
    var divOfNote = document.createElement('div');
    divOfNote.id = 'note-' + note.id;
    //divOfNote.classList.add('notes', 'shad', 'bord');
    divOfNote.innerHTML =
        '<div class="close-note" style="text-align:right;">\
        <img src="../img/croix.jpg" style="width:20px;height:20px"></div>\
        <table><tbody><tr><td colspan="3" class="nom"><span>' + note.name + '</span></td>\
        </tr><tr><td colspan="3" class="user"><span>' + note.user + '</span></td>\
        </tr><tr><td></td></tr><tr><td class="date">' + note.date.toLocaleDateString('fr-FR') + '&nbsp' + note.date.toLocaleTimeString('fr-FR') + '</td>\
        </tr><tr><td></td></tr><tr><td colspan="3" class="description"><span>' + note.desc + '</span>\
        </td></tr></tbody></table>';
    divOfNote.querySelector('.close-note img').addEventListener('click', noteremove);
    document.querySelector("#ListeNotes").append(divOfNote);
}

function afficheNotes() {
    var rest = new RestCRUD(ADRESSE_REST_SRV);
    var notes = rest.read();
    if (undefined !== notes)
        notes.forEach(element => {
            makeNote(parseJSONNote({ obj: element }));
        });
}

/**
 * 
 * @param {*} fdg sggdhdh
 */
function noteremove(sender) {
    // var note = document.querySelector('#note-1');
    // note.remove();
    if (window.confirm("Do you really want to delete this note?" + sender.target.parentElement.innerText)) {
        (new RestCRUD()).delete(sender.target.parentElement.parentElement.id, function(response) { sender.target.parentElement.parentElement.remove(); });
        //sender.target.parentElement.parentElement.remove();
    }
}

function submitForm(evt) {
    // if (document.querySelector('#note-name').innerText != '') {
    //     document.querySelector('#editionNote').style.display = 'none';
    var form = document.forms.monForm;
    var note = new Note(document.forms["formulaire"]['note-id'].value,
        document.forms["formulaire"]['note-name'].value,
        document.forms["formulaire"]['note-date'].value + ' ' + document.forms["formulaire"]['note-time'].value,
        document.forms["formulaire"]['note-desc'].value,
        document.forms["formulaire"]['note-user'].value);
    (new RestCRUD(ADRESSE_REST_SRV)).create({
        obj: note,
        function(response) {
            parseJSONNote({ jsonstr: response });
        }
    });
    //submitForm;
    resetForm;
    console.log('OK');
    jsinfo('Formulaire validé', 'GREEN');
    // } else {
    //     jsinfo('Saisir une note avant de valider', 'RED');
    //}
}

function resetForm() {
    document.forms[0].reset();
    document.querySelector('#editionNote').style.display = 'block';
    jsinfo('Formulaire reset', 'GREEN');
}

function defineEventOnElement(evt) {
    // var note1 = document.querySelector('#note-1 div.close-note img');
    // note1.addEventListener('click', noteremove);
    // var buttonReset = document.querySelector('#buttons > button:nth-child(2)');;
    // buttonReset.addEventListener('click', resetForm);
    //var buttons = document.forms[0].querySelectorAll('button');
    var buttons = document.querySelectorAll('form+div>button');
    buttons[0].addEventListener('click', submitForm);
    buttons[1].addEventListener('click', resetForm);
    document.forms["formulaire"].querySelector('select').addEventListener('change',
        onchangeselectuser);
    (new RestCRUD(ADRESSE_REST_SRV)).read();
}

function onchangeselectuser(evt) {
    console.log(evt.target, evt.target.item(evt.target.selectedIndex).value);
    if (evt.target.selectedIndex >= 0) {
        var imgPath = 'img/user/' + evt.target.item(evt.target.selectedIndex).value;

    } else
        var imgPath = 'img/user/defaultUser.jpg';
    document.querySelector('form .note-user-img').src = imgPath;
}


document.body.onload = defineEventOnElement;
document.querySelector('form select').selectedIndex = -1;
//document.querySelector('formulaire note-date').value = new Date("10/10/2019");

// function soap() {
//     var xmlhttp = new XMLHttpRequest();

//     //replace second argument with the path to your Secret Server webservices
//     xmlhttp.open('POST', '/secretserver/webservices/sswebservice.asmx', true);

//     //create the SOAP request
//     //replace username, password (and org + domain, if necessary) with the appropriate info
//     var strRequest =
//         '<?xml version="1.0" encoding="utf-8"?>' +
//         '<soap:Envelope ' +
//         'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"" ' +
//         'xmlns:xsd="http://www.w3.org/2001/XMLSchema"" ' +
//         'xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'; +
//     '<soap:Body>' +
//     '<Authenticate xmlns="urn:ws.plan-international.fr">' +
//     '<username>username</username>' +
//     '<password>password</password>' +
//     '<organization></organization>' +
//     '<domain></domain>' +
//     '</Authenticate>' +
//     '</soap:Body>' +
//     '</soap:Envelope>';

//     //specify request headers
//     xmlhttp.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');
//     xmlhttp.setRequestHeader('SOAPAction', '"urn:thesecretserver.com/Authenticate"';);

//     //FOR TESTING: display results in an alert box once the response is received
//     xmlhttp.onreadystatechange = function() {
//         if (xmlhttp.readyState == 4) {
//             alert(xmlhttp.responseText);
//         }
//     };

//     //send the SOAP request
//     xmlhttp.send(strRequest);
// };